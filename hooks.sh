#!/bin/bash

gp await-port 23000

_exts=(
    eamodio.gitlens
    esbenp.prettier-vscode
    hashicorp.terraform
    huizhou.githd
    kylepaulsen.stretchy-spaces
    mohd-akram.vscode-html-format
    ms-azuretools.vscode-docker
    ms-kubernetes-tools.vscode-kubernetes-tools
    ms-python.python
    rebornix.ruby
    redhat.vscode-xml
    redhat.vscode-yaml
    reduckted.vscode-gitweblinks
    github.copilot
)
for _ext in "${_exts[@]}"; do {
   code --install-extension "$_ext";
} done
